package com.fr.sdv.evaljava;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fr.sdv.evaljava.todolist.controllers.TodoListController;
import com.fr.sdv.evaljava.todolist.models.Todo;
import com.fr.sdv.evaljava.todolist.repositories.TodoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TodoListControllerTest {

    private MockMvc mockMvc;

    @Mock
    private TodoRepository todoRepository;

    @InjectMocks
    private TodoListController todoListController;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(todoListController).build();
    }

    @Test
    public void testGetAllTodo() throws Exception {
        // Mocking data
        Todo todo1 = new Todo(157, "Task 1", "Description 1", false);
        Todo todo2 = new Todo(28, "Task 2", "Description 2", true);
        when(todoRepository.findAll()).thenReturn(Arrays.asList(todo1, todo2));

        // Perform GET request
        mockMvc.perform(get("/todolist"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].title").value("Task 1"))
                .andExpect(jsonPath("$[0].description").value("Description 1"))
                .andExpect(jsonPath("$[0].completed").value(false))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].title").value("Task 2"))
                .andExpect(jsonPath("$[1].description").value("Description 2"))
                .andExpect(jsonPath("$[1].completed").value(true));

        // Verify that the repository method was called
        verify(todoRepository, times(1)).findAll();
    }

    @Test
    public void testAddTodo() throws Exception {
        // Mocking data
        Todo todoToAdd = new Todo(0, "New Task", "New Description", false);
        Todo savedTodo = new Todo(1, "New Task", "New Description",false);
        when(todoRepository.save(todoToAdd)).thenReturn(savedTodo);

        // Perform POST request
        mockMvc.perform(post("/todolist")
                .content(asJsonString(todoToAdd))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.title").value("New Task"))
                .andExpect(jsonPath("$.description").value("New Description"))
                .andExpect(jsonPath("$.completed").value(false));

        // Verify that the repository method was called
        verify(todoRepository, times(1)).save(todoToAdd);
    }

    @Test
    public void testMarkTodoAsComplete() throws Exception {
        // Mocking data
        Todo todoToComplete = new Todo(15, "Task 1", "Description 1", false);
        when(todoRepository.findById(1L)).thenReturn(Optional.of(todoToComplete));

        // Perform PUT request
        mockMvc.perform(put("/todolist/1/complete"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.title").value("Task 1"))
                .andExpect(jsonPath("$.description").value("Description 1"))
                .andExpect(jsonPath("$.completed").value(true));

        // Verify that the repository method was called
        verify(todoRepository, times(1)).findById(1L);
        verify(todoRepository, times(1)).save(todoToComplete);
    }

    @Test
    public void testDeleteTodo() throws Exception {
        // Mocking data
        when(todoRepository.existsById(1L)).thenReturn(true);

        // Perform DELETE request
        mockMvc.perform(delete("/todolist/1"))
                .andExpect(status().isNoContent());

        // Verify that the repository method was called
        verify(todoRepository, times(1)).existsById(1L);
        verify(todoRepository, times(1)).deleteById(1L);
    }

    // Helper method to convert object to JSON string
    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
package com.fr.sdv.evaljava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaljavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaljavaApplication.class, args);
	}

}

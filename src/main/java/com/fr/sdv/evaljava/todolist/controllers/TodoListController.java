package com.fr.sdv.evaljava.todolist.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.fr.sdv.evaljava.todolist.models.Todo;
import com.fr.sdv.evaljava.todolist.services.TodoListService;

@RestController
@RequestMapping("/todolist")
public class TodoListController {

    private final TodoListService todoListService;


    public TodoListController( TodoListService todoListService) {
        this.todoListService = todoListService;
    }

    // Récupérer toutes les tâches
    @GetMapping
    public ResponseEntity<List<Todo>> getAllTodo() {
        List<Todo> todoList = todoListService.getAllTodo();
        return new ResponseEntity<>(todoList, HttpStatus.OK);
    }

    // Ajouter une nouvelle tâche
    @PostMapping
    public ResponseEntity<Todo> addTodo(@RequestBody Todo todo) {
        Todo newTodo = todoListService.addTodo(todo);
        return new ResponseEntity<>(newTodo, HttpStatus.CREATED);
    }

    // Marquer une tâche comme complète
    @PutMapping("/{todoId}/complete")
    public ResponseEntity<Todo> markTodoAsComplete(@PathVariable Long todoId) {
        return todoListService.markTodoAsComplete(todoId);
    }

    // Supprimer une tâche
    @DeleteMapping("/{todoId}")
    public ResponseEntity<Void> deleteTodo(@PathVariable Long todoId) {
        return todoListService.deleteTodo(todoId);
    }

   
}

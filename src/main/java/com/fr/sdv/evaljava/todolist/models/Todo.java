package com.fr.sdv.evaljava.todolist.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "todo")

public class Todo {
    @Id
    @GeneratedValue
    private long id;

    private String title;
    private String description;
    private boolean completed = false;

}
package com.fr.sdv.evaljava.todolist.services;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fr.sdv.evaljava.todolist.models.Todo;
import com.fr.sdv.evaljava.todolist.repositories.TodoRepository;

@Service
public class TodoListService {

    private final TodoRepository todoRepo;

    @Autowired
    public TodoListService(TodoRepository todoRepo) {
        this.todoRepo = todoRepo;
    }

    public List<Todo> getAllTodo() {
        return todoRepo.findAll();
    }

    public Todo addTodo(Todo todo) {
        return todoRepo.save(todo);
    }

    public ResponseEntity<Todo> markTodoAsComplete(Long todoId) {
        Optional<Todo> todoExist = todoRepo.findById(todoId);

        if (todoExist.isPresent()) {
            Todo todoToComplete = todoExist.get();
            todoToComplete.setCompleted(true);
            todoRepo.save(todoToComplete);
            return  new ResponseEntity<>(todoToComplete, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Void> deleteTodo(Long todoId) {
        if (todoRepo.existsById(todoId)) {
            todoRepo.deleteById(todoId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}